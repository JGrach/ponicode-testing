const rewire = require("rewire")
const _test = rewire("./toTest")
const add = _test.__get__("add")
const addDeScope = _test.__get__("addDeScope")
const addToAbove = _test.__get__("addToAbove")
const AdderPrototype = _test.__get__("AdderPrototype")
const addArrayRef = _test.__get__("addArrayRef")
const addAndReturnArrayRef = _test.__get__("addAndReturnArrayRef")
const addArrayNew = _test.__get__("addArrayNew")
const toTest = rewire("./toTest")
const addPromise = toTest.__get__("addPromise")
// @ponicode
describe("add", () => {
    test("0", () => {
        let param1 = -1.0
        let result = add(param1, 10.23)
        expect(result).toBe(9.23)
        expect(param1).toBe(-1)
    })

    test("1", () => {
        let result = add(-29.45, -29.45)
        expect(result).toBe(-58.9)
    })

    test("2", () => {
        let result = add(0.2, 10)
        expect(result).toBe(10.2)
    })

    test("3", () => {
        let result = add(1.5, 10)
        expect(result).toBe(11.5)
    })

    test("4", () => {
        let result = add(2, 10)
        expect(result).toBe(12)
    })

    test("5", () => {
        let result = add(0.5, 10)
        expect(result).toBe(10.5)
    })
})

// @ponicode
describe("addDeScope", () => {
    test("0", () => {
        let result = addDeScope(-29.45)
        expect(result).toBe(-27.45)
    })

    test("1", () => {
        let result = addDeScope(0.5)
        expect(result).toBe(2.5)
    })

    test("2", () => {
        let result = addDeScope(-0.5)
        expect(result).toBe(1.5)
    })

    test("3", () => {
        let result = addDeScope(10.23)
        expect(result).toBe(12.23)
    })
})

// @ponicode
describe("addToAbove", () => {
    test("0", () => {
        let result = addToAbove(-29.45, -29.45)
        expect(result).toBe(-58.9)
    })

    test("1", () => {
        let result = addToAbove(-0.5, -0.5)
        expect(result).toBe(-1)
    })

    test("2", () => {
        let param1 = 0.0
        let result = addToAbove(param1, -0.5)
        expect(result).toBe(-0.5)
        expect(param1).toBe(0)
    })

    test("3", () => {
        let result = addToAbove(0.5, 10.23)
        expect(result).toBe(10.73)
    })

    test("4", () => {
        let param1 = 10.0
        let param2 = -1.0
        let result = addToAbove(param1, param2)
        expect(result).toBe(9)
        expect(param1).toBe(10)
        expect(param2).toBe(-1)
    })
})

// @ponicode
describe("AdderPrototype", () => {
    test("0", () => {
        let result = AdderPrototype(-29.45)
        expect(result).toBe(undefined)
    })
})

// @ponicode
describe("addArrayRef", () => {
    test("0", () => {
        let param1 = [1, 2, 3]
        let result = addArrayRef(param1, -29.45)
        expect(result).toBe(4)
        expect(param1).toEqual([1, 2, 3, -29.45])
    })

    test("1", () => {
        let param1 = [1, 2, 3]
        let result = addArrayRef(param1, -0.5)
        expect(result).toBe(4)
        expect(param1).toEqual([1, 2, 3, -0.5])
    })
})

// @ponicode
describe("addAndReturnArrayRef", () => {
    test("0", () => {
        let param1 = [1, 2, 3]
        let result = addAndReturnArrayRef(param1, -29.45)
        expect(result).toEqual([1, 2, 3, -29.45])
        expect(param1).toEqual([1, 2, 3, -29.45])
    })

    test("1", () => {
        let param1 = [1, 2, 3]
        let param2 = 10.0
        let result = addAndReturnArrayRef(param1, param2)
        expect(result).toEqual([1, 2, 3, 10])
        expect(param1).toEqual([1, 2, 3, 10])
        expect(param2).toBe(10)
    })

    test("2", () => {
        let param1 = [1, 2, 3]
        let param2 = 0.0
        let result = addAndReturnArrayRef(param1, param2)
        expect(result).toEqual([1, 2, 3, 0])
        expect(param1).toEqual([1, 2, 3, 0])
        expect(param2).toBe(0)
    })
})

// @ponicode
describe("addArrayNew", () => {
    test("0", () => {
        let param1 = [1, 2, 3]
        let result = addArrayNew(param1, -29.45)
        expect(result).toEqual([1, 2, 3, -29.45])
        expect(param1).toEqual([1, 2, 3])
    })

    test("1", () => {
        let param1 = [1, 2, 3]
        let param2 = -1.0
        let result = addArrayNew(param1, param2)
        expect(result).toEqual([1, 2, 3, -1])
        expect(param1).toEqual([1, 2, 3])
        expect(param2).toBe(-1)
    })
})

// @ponicode
describe("addPromise", () => {
    test("0", async () => {
        let result = await addPromise(2, 2)
        expect(result).toBe(4)
    })

    test("1", async () => {
        let result = await addPromise(3, 2)
        expect(result).toBe(5)
    })
})
