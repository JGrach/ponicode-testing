// simple - it works
const add = (x, y) => x + y;

// promise - it works
const addPromise = async (x, y) => x + y;

// closure - cannot test return function
const addClosure = (x) => (y) => x + y;

// global scope - it works on a hardcoded x
const x = 2;
const addDeScope = (y) => x + y;

// global scope (mock ?) - resend math random each times, cannot mock z ?
const z = Math.random();
const addDeScopeDynamic = (y) => z + y;

// this - seems I cannot apply and even prototype doesn't work
function addThis(y) {
    return this.x + y
}
addThis.prototype.x = 2;

// call from an other - it works 
const addToAbove = (x, y) => add(x, y);

// constructor - seems need a return and "this" misunderstood 
function AdderPrototype(x) {
    this.x = x
    this.addToX = addThis.bind(this);
}

// constructor sugar syntax - no detection
class AdderClass {
    constructor(x){
        this.x = x
    }
    addToX = function (y) { x + y } 
}

// ------------
// Is this a trouble with reference return ?
// Try to work with array

// it works
const addArrayRef = (x, y) => x.push(y)

// it works - agnostic about reference, 
const addAndReturnArrayRef = (x, y) => {
    x.push(y)
    return x;
}

// it works - agnostic about reference
const addArrayNew = (x, y) => [...x, y]